let express = require('express')
let app = express()
let cors = require('cors')
let api = require('./routes')
let port = process.env.PORT || 8000

var whitelist = ['http://uchinka.gitlab.io', 'https://uchinka.gitlab.io']
var corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('error_cors'))
    }
  },
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions), (err, req, res, next) => {
  if (err.message !== 'error_cors') return next()
  res.json({
    'status': 'error',
    'message': 'Request not allowed by CORS'
  }, 403)
})
app.use('/api', api)
app.all('*', (req, res) => {
  res.json({
    'status': 'error',
    'message': 'Page not found'
  },404)
})
app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`)
})